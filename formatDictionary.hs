import Data.List
import Data.Char
import qualified Data.Set as Set
import System.Environment
main = do dict <- getContents
          let formattedDict = Set.toList . Set.fromList $ "a":"i":[map toLower words | words <- lines dict, length words > 1]
          putStr (unlines formattedDict)
